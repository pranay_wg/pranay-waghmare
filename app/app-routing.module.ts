import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComparisonComponent } from './comparison/comparison.component';
import { ContactusComponent } from './contactus/contactus.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { ReturnexComponent } from './returnex/returnex.component';
import { ShippingComponent } from './shipping/shipping.component';
import { SignupComponent } from './signup/signup.component';
import { TermscondComponent } from './termscond/termscond.component';
import { TrackorderComponent } from './trackorder/trackorder.component';

const routes: Routes = [
  {path:'', component:IndexComponent},
  {path:'home',component:HomeComponent},
  {path:'header', component:HeaderComponent},
  {path:'login',component:LoginComponent},
  {path:'returnex', component:ReturnexComponent},
   {path:'shipping',component:ShippingComponent},
   {path:'tremscond', component:TermscondComponent},
   {path:'privacy', component:PrivacypolicyComponent}, 
   {path:'trackorder', component:TrackorderComponent}, 
   {path:'comparison', component:ComparisonComponent},                   
  {path:'signup',component:SignupComponent},
  {path:'contact', component:ContactusComponent}
   
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
