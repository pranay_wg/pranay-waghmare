import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopcbComponent } from './topcb.component';

describe('TopcbComponent', () => {
  let component: TopcbComponent;
  let fixture: ComponentFixture<TopcbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopcbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopcbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
