import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnexComponent } from './returnex.component';

describe('ReturnexComponent', () => {
  let component: ReturnexComponent;
  let fixture: ComponentFixture<ReturnexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReturnexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
