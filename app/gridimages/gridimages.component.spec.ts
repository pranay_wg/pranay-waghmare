import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridimagesComponent } from './gridimages.component';

describe('GridimagesComponent', () => {
  let component: GridimagesComponent;
  let fixture: ComponentFixture<GridimagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridimagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
