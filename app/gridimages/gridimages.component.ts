import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gridimages',
  templateUrl: './gridimages.component.html',
  styleUrls: ['./gridimages.component.css']
})
export class GridimagesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  tiles: Tile[] = [
    {text: 'One', cols: 1, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 1, rows: 2, color: '#DDBDF1'},
    {text: 'Five', cols: 1, rows: 2, color: 'slateblue'},
    {text: 'six', cols: 1, rows: 1, color: 'red'},
  ];



}

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}


 
