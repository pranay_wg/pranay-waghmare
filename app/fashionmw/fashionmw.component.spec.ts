import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionmwComponent } from './fashionmw.component';

describe('FashionmwComponent', () => {
  let component: FashionmwComponent;
  let fixture: ComponentFixture<FashionmwComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionmwComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionmwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
