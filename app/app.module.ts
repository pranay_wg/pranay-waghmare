import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
//import { from } from 'rxjs';
import {MatToolbarModule} from '@angular/material/toolbar';
import { HeaderComponent } from './header/header.component';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import { FooterComponent } from './footer/footer.component';
import { SignupComponent } from './signup/signup.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { TopcbComponent } from './topcb/topcb.component';
import { HomeproductsComponent } from './homeproducts/homeproducts.component';
import {MatTabsModule} from '@angular/material/tabs';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { SummerComponent } from './summer/summer.component';
import { SpecialdealComponent } from './specialdeal/specialdeal.component';
import { FeaturedComponent } from './featured/featured.component';
import { FashionmwComponent } from './fashionmw/fashionmw.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TopsellingComponent } from './topselling/topselling.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PageviewComponent } from './pageview/pageview.component';
import { GridimagesComponent } from './gridimages/gridimages.component';
import { ReturnexComponent } from './returnex/returnex.component';
import { ShippingComponent } from './shipping/shipping.component';
import { TermscondComponent } from './termscond/termscond.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TrackorderComponent } from './trackorder/trackorder.component';
import { ComparisonComponent } from './comparison/comparison.component';
import { HomeComponent } from './home/home.component';
import { IndexComponent } from './index/index.component';










@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    SignupComponent,
    TopcbComponent,
    HomeproductsComponent,
    SubscribeComponent,
    SummerComponent,
    SpecialdealComponent,
    FeaturedComponent,
    FashionmwComponent,
    TopsellingComponent,
    ContactusComponent,
    PageviewComponent,
    GridimagesComponent,
    ReturnexComponent,
    ShippingComponent,
    TermscondComponent,
    PrivacypolicyComponent,
    TrackorderComponent,
    ComparisonComponent,
    HomeComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatListModule,
    MatSidenavModule,
    MatIconModule,
    MatSelectModule,
    MatGridListModule,
    MatTabsModule,
    MatButtonToggleModule,
  
      BrowserAnimationsModule,
      /*
      FormsModule,
      ReactiveFormsModule
      MatToolbarModule,
      MatIconModule,
      MatSidenavModule,
      MatListModule,
      MatButtonModule,
      MatInputModule,
      MatGridListModule,
      MatCardModule,
      FlexLayoutModule,
      MatSliderModule,
      NgbModule,
      MatMenuModule    */




  

    ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
